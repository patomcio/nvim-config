require 'plugin.packer'
require 'plugin.config'

require 'core.options'
require 'core.keymaps'
require 'core.colorscheme'
