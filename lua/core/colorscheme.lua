vim.cmd('colorscheme jellybeans')

local transparent = {
  'Normal',
  'NormalNC',
  'Comment',
  -- 'Constant',
  'Special',
  -- 'Identifier',
  -- 'Statement',
  'PreProc',
  -- 'Type',
  -- 'Underlined',
  'Todo',
  -- 'String',
  -- 'Function',
  -- 'Conditional',
  -- 'Repeat',
  -- 'Operator',
  -- 'Structure',
  'LineNr',
  'CursorLineNr',
  'NonText',
  'SignColumn',
  'EndOfBuffer',
  'GitSignsAdd',
  'GitSignsChange',
  'GitSignsDelete',
  'vimSynMatchRegion',
  'VertSplit',
  'CursorLine',
  'NormalFloat',
  'TabLine',
  'TabLineFill',
  'TabLineSel',
  'CmpNormal',
  'CmpDocNormal',
}

for _, item in ipairs(transparent) do
  vim.cmd('hi ' .. item .. ' ctermbg=NONE guibg=NONE')
end

local colored = {
  CursorLineNr = 'guifg=goldenrod gui=bold',
  LineNr = 'guifg=gray20',
  Visual = 'guibg=gray30',
  CursorLine = 'guibg=gray20',
  GitSignsAdd = 'gui=bold guifg=PaleGreen3',
  GitSignsDelete = 'gui=bold guifg=firebrick1',
  GitSignsChange = 'gui=bold guifg=DarkSlateGray4',
  EndOfBuffer = 'guifg=gray40',
}

for item, color in pairs(colored) do
  vim.cmd('hi ' .. item .. ' ' .. color)
end

vim.cmd("let &fcs='eob: '")
