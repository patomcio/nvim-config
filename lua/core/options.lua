local options = {
  clipboard = 'unnamedplus',
  mouse = 'a',
  showtabline = 2,

  showmode = false,
  showcmd = true,
  laststatus = 2,

  conceallevel = 0,
  smartcase = true,
  smartindent = true,

  splitbelow = true,
  splitright = true,

  swapfile = true,
  undofile = true,

  tabstop = 4,
  shiftwidth = 4,
  expandtab = true,
  backspace = '2',

  cursorline = true,
  autoread = true,
  autowrite = true,

  relativenumber = true,
  number = true,
  numberwidth = 4,

  wrap = false,
  linebreak = true,

  scrolloff = 8,
  sidescrolloff = 8,

  termguicolors = true,
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

vim.cmd [[
  au BufRead *.md set wrap
]]

vim.cmd [[
  set formatoptions-=cro
]]

