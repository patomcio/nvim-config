vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

keymap('v', '<', '<gv', opts)
keymap('v', '>', '>gv', opts)
keymap('v', 'p', "'_dP", opts)
keymap('v', '<A-j>', ":move '>+1<CR>gv=gv", opts)
keymap('v', '<A-k>', ":move '<-2<CR>gv=gv", opts)

keymap('n', 'j', 'gj', opts)
keymap('n', 'k', 'gk', opts)

keymap('n', '<C-j>', ':tabp<CR>', opts)
keymap('n', '<C-k>', ':tabn<CR>', opts)
keymap('n', '<C-n>', ':tabnew<CR>', opts)

-- Neoformat

keymap('n', '<C-y>', ':Neoformat<CR>', opts)

-- Neotree

keymap('n', '<F1>', ':Neotree toggle<CR>', opts)

-- Telescope

local telescope = require('telescope.builtin')

vim.keymap.set('n', '<leader>tf', telescope.find_files, opts)
vim.keymap.set('n', '<leader>tg', telescope.live_grep, opts)
vim.keymap.set('n', '<leader>tt', ':TodoTelescope<CR>', opts)

