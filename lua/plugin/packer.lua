local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end

  return false
end

vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost packer.lua source <afile> | PackerSync
  augroup end
]]

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim' -- required

  -- Mason and LSP
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'
  use 'neovim/nvim-lspconfig'

  -- Snippets/Auto complete
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'L3MON4D3/LuaSnip'
  use 'onsails/lspkind.nvim'
  use 'saadparwaiz1/cmp_luasnip'
  use 'rafamadriz/friendly-snippets'

  -- Utils
  use 'nvim-treesitter/nvim-treesitter' -- Syntax Highlight
  use {
    'nvim-telescope/telescope.nvim', -- Fuzzy Finder
    tag = '0.1.6',
    requires = {{'nvim-lua/plenary.nvim'}}
  }
  use {
    'nvim-neo-tree/neo-tree.nvim', -- File tree
    branch = 'v3.x',
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons',
      'MunifTanjim/nui.nvim',
    }
  }
  use 'nvim-lualine/lualine.nvim' -- Status line
  use 'lewis6991/gitsigns.nvim' -- Git thingys in code
  use 'sbdchd/neoformat' -- Code Formatter

  -- Visual
  use 'rafi/awesome-vim-colorschemes' -- cool colors
  use 'nanozuki/tabby.nvim' -- cool looking tabs
  use {
    'folke/todo-comments.nvim', -- cool todo comments?
    dependencies = { "nvim-lua/plenary.nvim" },
  }

  -- Other
  use 'goolord/alpha-nvim' -- Greeter
  use 'andweeb/presence.nvim' -- Discord RPC (Rich Presence)
  use 'wakatime/vim-wakatime' -- Metrics for Neovim

  if packer_bootstrap then
    require('packer').sync()
  end
end)
