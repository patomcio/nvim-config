local langs = {
  'lua',
  'python',
  'html', 'css', 'scss', 'astro', 'svelte', 'vue',
  'javascript', 'typescript', 'prisma',
  'c', 'c_sharp', 'cpp', 'cmake',
  'go', 'gomod', 'gosum',
  'rust',
  'ocaml',
  'kotlin',
  'markdown', 'markdown_inline',
  'xml', 'yaml', 'json',
  'bash', 'dockerfile',
  'php',
  'zig',
};

require('nvim-treesitter.configs').setup({
  ensure_installed = langs,
  sync_install = false,
  auto_install = true,
  highlight = {
    enable = true,
  },
})
