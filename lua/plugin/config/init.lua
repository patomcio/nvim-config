require 'plugin.config.lualine'
require 'plugin.config.neotree'
require 'plugin.config.gitsigns'
require 'plugin.config.treesitter'
require 'plugin.config.lsp'
require 'plugin.config.completions'
require 'plugin.config.neoformat'
require 'plugin.config.tabby'
require 'plugin.config.alpha'
require 'plugin.config.todo-comments'
require 'plugin.config.telescope'

