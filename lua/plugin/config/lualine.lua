local custom_nightfly = require 'lualine.themes.nightfly'

custom_nightfly.normal.b.bg = 'NONE'
custom_nightfly.normal.c.bg = 'NONE'
custom_nightfly.inactive.c.bg = 'NONE'

require('lualine').setup({
  options = {
    icons_enabled = true,
    theme = custom_nightfly,
    disabled_filetypes = { 'neo-tree' },
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch'},
    lualine_c = {
      {
        'filename',
        path = 1,
      },
    },
    lualine_x = {'diagnostics', 'filetype'},
    lualine_y = {},
    lualine_z = {},
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {
      {
        'filename',
        path = 1,
      },
    },
    lualine_x = {'diagnostics', 'filetype'},
    lualine_y = {},
    lualine_z = {},
  },
})
