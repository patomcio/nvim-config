local alpha = require("alpha")
local dashboard = require("alpha.themes.dashboard")

local function split(inputstr)
  local sep = "\n"
  local t = {}

  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    table.insert(t, str)
  end

  return t
end

dashboard.section.header.val = split([[
                    ++++++++++++
                 ++++++++++++++++++
               ++++++++++++++++++++++
              ++++++++++++++++++++++++ + ++++
             ++++++++++++++++++++++++ +++ ++++++
             ++++++++++++++++++++++++++++++++++++
              ++++++++++++++++++++++++++++++++++++
              :::::::::,a@@a,:::::,a@a,++++++++++.
         .ooOOOOOOOOOOo@@@@@@oOoOo@@@@@,++++++++/:.
      o OOOOOOOOOOOOo@@@@@@@@@oOOo@@@@@@,++++++/:::
   o oOOOOOOOOOOOOOo@@@@@@@@@@@oOo@@@@@@a  ':::::::
  oOoOOOOOOOOOOOOOOo@@@@@@@@@@@oOo@@@@@@@   :::::::                                   ..
 oOOOOOOOOOOOOOOOOo@@@@@@@@@@@@oOo@@@@@@@   ::: ::'          `oOo.   `oO:'            oo
 oOOOOOOOOOOOOOOOOo`  '@@@@@@@@oOo` '@@@@  ,:'  '              OOo.    O
 oOOOOOOO%%%%%OOOOo    @@@@@@@@oOo   @@@.                      O %O.   O `oO'   `O:'`oOO  `oOOpOOO..pOOO..
  oOOOO;%%%.%%%OOOo.  ,@@@@@@@oOOo. ,@@@'                      O  `Oo. O   @.   ,@    OO    OO    OO    OO
   oOOO%%%.%%%%%OOOo.@@@@@@@@oOOOo@@@@@'                       O   `OO.O    @. ,@     OO    OO    OO    OO
    OOO%%%.%%%%%%OOo@@@@@@@@oOOOOOo@@@'        .,;%%%%%;.      O     %OO     @@@      OO    OO    OO    OO
     OOO%%.%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//%%%%%%%%%    .oO..    %O      %     .oOO...oOO.  oOO.  oOO.
       OO%%.%%%%%%%%%%%%%%%%;%%%%%%%%%%%%%%//%%%%%%%%%%%;
         O%%.....';%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%;'                       patomcio edition
           %%.............%%%%%%%%%%%%%%%%%%%%%%%;'
            %%............`%%,   """""""""""""
             %%............%%;
              %%...........%%;
               %%%%%%%%%%%%%;
                `%%%%%%%%%;'
]])

dashboard.section.buttons.val = {
    dashboard.button( "e", "  > New file" , ":new<CR>"),
    dashboard.button( "E", "  > Open file explorer" , ":Neotree<CR>"),
    dashboard.button( "f", "  > Find file", ":Telescope find_files<CR>"),
    dashboard.button( "r", "  > Recent"   , ":Telescope oldfiles<CR>"),
    dashboard.button( "q", "  > Quit NVIM", ":qa!<CR>"),
}

alpha.setup(dashboard.opts)

vim.cmd([[
    autocmd FileType alpha setlocal nofoldenable
]])
