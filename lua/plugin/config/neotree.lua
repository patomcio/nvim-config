local tree = require('neo-tree')

tree.setup({
  enable_git_status = true,
  default_component_configs = {
    indent = {
      indent_size = 2,
      padding = 2,
      with_markers = true,
      indent_marker = "│",
      last_indent_marker = "└",
      highlight = "NeoTreeIndentMarker",
      with_expanders = true,
      expander_collapsed = "",
      expander_expanded = "",
      expander_highlight = "NeoTreeExpander",
    },
    icon = {
      folder_closed = "",
      folder_open = "",
      folder_empty = "󰜌",
      default = "*",
      highlight = "NeoTreeFileIcon"
    },
    git_status = {
      symbols = {
        added     = "✚",
        modified  = "",
        deleted   = "✖",
        renamed   = "󰁕",
        untracked = "",
        ignored   = "",
        unstaged  = "",
        staged    = "",
        conflict  = "",
      },
    },
  },
  filesystem = {
    filtered_items = {
      visible = true,
    },
    follow_current_file = {
        enabled = true,
        leave_dirs_opes = true
    }
  },
  buffers = {
    follow_current_file = {
        enabled = true,
        leave_dirs_opes = true
    }
  },
  source_selector = {
    winbar = false,
    statusline = false,
  },
  event_handlers = {
    {
      event = 'file_opened',
      handler = function()
        tree.close_all()
      end
    },
  },
  window = {
    position = "right",
    mappings = {
      ["P"] = { "toggle_preview", config = { use_float = true, use_image_nvim = true, } },
    }
  }
})
