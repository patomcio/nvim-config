local theme = {
  fill = 'Normal',
  head = { bg = 'NONE', fg = '#ffffff' },
  current_tab = { bg = 'NONE', fg = '#ffffff' },
  tab = { bg = 'NONE', fg = '#555555' },
  win = { bg = 'NONE', fg = '#ffffff' },
  tail = { bg = 'NONE', fg = '#ffffff' },
}

require('tabby.tabline').set(function(line)
  return {
    {
      { '  ', hl = theme.head },
      line.sep('   ', theme.fill, theme.fill),
    },
    line.tabs().foreach(function(tab)
      local hl = tab.is_current() and theme.current_tab or theme.tab
      return {
        (tab.is_current() and '➤' or ' '),
        tab.name(),
        tab.close_btn(''),
        line.sep(' ', theme.fill, theme.fill),
        hl = hl,
        margin = ' ',
      }
    end),
    line.spacer(),
    {
      { ' 󰣇 in arch btw ', hl = theme.tail },
    },
    hl = theme.fill,
  }
end)
