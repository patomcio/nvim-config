local lsps = {
  'svelte',
  'astro',
  'cmake',
  'prismals',
  'clangd',
  'csharp_ls',
  'tailwindcss',
  'ts_ls',
  'pyright',
  'rust_analyzer',
  'phpactor',
  'gopls',
  'lua_ls',
  'cssls',
  'emmet_ls',
  'html',
  'jsonls',
  'marksman',
  'yamlls',
  'zls',
  'volar',
  'kotlin_language_server',
  'jdtls',
}

require('mason').setup()
require('mason-lspconfig').setup({
  ensure_installed = lsps
})

local border = {
  {"╭", "FloatBorder"},
  {"─", "FloatBorder"},
  {"╮", "FloatBorder"},
  {"│", "FloatBorder"},
  {"╯", "FloatBorder"},
  {"─", "FloatBorder"},
  {"╰", "FloatBorder"},
  {"│", "FloatBorder"},
}

local signs = {
  { name = "DiagnosticSignError", text = "" },
  { name = "DiagnosticSignWarn",  text = "" },
  { name = "DiagnosticSignHint",  text = "" },
  { name = "DiagnosticSignInfo",  text = "" },
}

local config = {
  virtual_text = false,
  signs = { active = signs },
  update_in_insert = true,
  underline = true,
  severity_sort = true,
  float = {
    focusable = true,
    style = 'minimal',
    border = border,
    source = 'always',
    header = '',
    prefix = '',
  },
}

vim.diagnostic.config(config)

local handlers =  {
  ["textDocument/hover"] =  vim.lsp.with(vim.lsp.handlers.hover, { border = border }),
  ["textDocument/signatureHelp"] =  vim.lsp.with(vim.lsp.handlers.signature_help, { border = border }),
}

local function on_attach()
  local opts = { noremap = true, silent = true }
  vim.keymap.set('n', '<F2>', vim.lsp.buf.rename, opts)
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, opts)

  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
  vim.keymap.set('n', 'gl', vim.diagnostic.open_float, opts)
end

local capabilities = require('cmp_nvim_lsp').default_capabilities()

for _, lsp in pairs(lsps) do
  require('lspconfig')[lsp].setup({
    on_attach = on_attach,
    capabilities = capabilities,
    handlers = handlers,
  })
end


